const configUtil = require('./congifFileUtil');
const axios = require('axios');
const express = require('express');
const path = require('path');
const app = express();

const port = configUtil.getAvailablePort();
const pathToStaticFiles = path.join(__dirname, '../client/dist');

const respondToHealthRequest = (req, res) => {
    const health = Math.ceil(Math.random() * 10);
    res.status(200).send(String(health));
};

const getConfigInterval = (req, res) => {
    const interval = configUtil.getInterval();
    res.status(200).json(interval);
};

const getHealthFromProcess = async (ip, port) => {
    try {
        const health = await axios.get(`http://${ip}:${port}/respondToHealthRequest`);
        return health.data;
    }
    catch (ex) {
        //console.error('getHealthFromProcess failed', ex);
        return 'timeout';
    }
};

const getHealthFromAllProcess = async (req, res) => {
    const servers = configUtil.getServers();
    const healthFromAllProcesses = await Promise.all(servers.map(async server => {
        const health = await getHealthFromProcess(server.ip, server.port);
        return {
            ...server,
            health,
            timeStamp: Date.now()
        };
    }));
    healthFromAllProcesses.sort((a, b) => {
        return a.timeStamp - b.timeStamp;
    });
    res.status(200).json(healthFromAllProcesses);
};

//ROUTES
app.get('/respondToHealthRequest', respondToHealthRequest);
app.get('/getConfigInterval', getConfigInterval);
app.get('/getHealthFromAllProcess', getHealthFromAllProcess);

app.use(express.static(pathToStaticFiles));
//if there is a process available based on the config file
if (port) {
    app.listen(port, () => console.log(`app listening on port ${port}...`))
}
