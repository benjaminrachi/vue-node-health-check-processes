const CONFIG_PATH = './config.json';
const config = require(CONFIG_PATH);
const fs = require('fs');

const getConfig = () => {
    return config;
};

const getServers = () => {
    return config.servers || [];
};

const getInterval = () => {
    return config.interval;
};

const saveServerConfig = (serverConfig) => {
    const serverIndex = config.servers.findIndex(server => server.uid === serverConfig.uid);
    config.servers[serverIndex] = serverConfig;
    fs.writeFileSync(CONFIG_PATH, JSON.stringify(config, null, 2));
};

const getAvailablePort = () => {
    const availableServer = getServers().find(server => server.free);
    if (availableServer) {
        availableServer.free = false;
        //write to the config file that this server is not free anymore
        saveServerConfig(availableServer);
        return availableServer.port;
    }
};

module.exports = {
    getConfig,
    getServers,
    getInterval,
    getAvailablePort,
};
