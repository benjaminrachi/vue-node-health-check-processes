import axios from 'axios';

const apiClient = axios.create({
    baseUrl: window.location.origin,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
});

export default {
    getConfigInterval() {
        return apiClient.get('getConfigInterval');
    },
    getHealthFromAllProcess() {
        return apiClient.get('getHealthFromAllProcess');
    }
}